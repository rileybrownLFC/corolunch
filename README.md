# CoroLunch

Welcome to CoroLunch! This is a simple python program to help Coronado HS students figure out which lunch hour they have.

# 1. Downloading Dependencies

All you need is VS Code, Python and Git installed on your computer. Most Linux distros come with these preinstalled, but Windows and Mac users may need to install them. You can check to see if Git is installed by typing `git --version` in your terminal. You can check to see if Python is installed by typing `python --version` in your terminal as well.

- **Install VS Code:** https://code.visualstudio.com/Download
- **Install Git:** https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
- **Install Python:** https://www.python.org/downloads/

# 2. Download the Program

1. Click on the blue "clone" button, and copy either of the links in the dropdown. I prefer HTTPS, but it is up to you. 
2. Navigate to your terminal and type in the following command: `$ git clone <url>`
    - "url" acting as the URL that you copied in step 1.
   - For example, you can type something like: `git clone https://gitlab.com/rileybrownLFC/corolunch.git`
3. Open the `CoroLunch.py` file in VS Code.
4. With `CoroLunch.py` file open in VS Code, click on the play button in the top right, and a terminal should pop up that allows you to interact with the program!

# Reporting Bugs

Report any bugs by opening an issue on this GitLab Repository. 
