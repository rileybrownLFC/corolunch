def CoroLunch():
    
    firstLunchClasses = ["art" , "consumer & family science" , "gradpoint" , "music" , "pe" , "health" , "science" , "social studies"]
    secondLunchClasses = ["applied technology" , "business" , "edge" , "english" , "math" , "sped" , "study hall" , "world languages"]
    
    print("Welcome to CoroLunch! This program helps determine what lunch Coronado HS students have.")
    begin = input("Click enter to begin: ")

    if (begin == ""):
        print("Here are the subjects offered at Coronado for reference: ")
        print(*firstLunchClasses, sep = "\n")
        print(*secondLunchClasses, sep = "\n")
        print("")
        
        thirdPeriod = input("What is your third period subject? See above for reference. ")

        if (thirdPeriod in firstLunchClasses):
            print("On Monday, Tuesday, and Thursday you have FIRST LUNCH!")
        else:
            print("On Monday, Tuesday, and Thursday you have SECOND LUNCH!")

        seventhPeriod = input("What is your seventh period subject? See above for reference: ")

        if (seventhPeriod in firstLunchClasses):
            print("On Wednesday and Friday you have FIRST LUNCH!")
        else:
            print("On Wednesday and Friday you have SECOND LUNCH!")


CoroLunch()